# YahooWeather

This Gem integrates some features from the public [Yahoo Weather API](https://developer.yahoo.com/weather)

It provides forecast/current conditions/wind conditions/one word conditions from any city in the world you want to check.

This gem is built in order to play with the API, as an experience. Feel free to fork your own version and adapt it to your needs.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'yahoo_weather', :git => "git://github.com/dantgn/yahoo_weather.git"

```

And then execute:

    $ bundle

## Usage

Create a `Station` object providing the city (or city and country for being more specific) you want to know the weather info:

```ruby
station = YahooWeather::Station.new('Tarragona')
station = YahooWeather::Station.new('Tarragona', 'Spain')
```

You can also provide the units in which you want to get the results (Celsius as default, but also Fahrenheit can be provided)
```ruby
station = YahooWeather::Station.new('London', 'UK', 'f')
station = YahooWeather::Station.new('Tarragona', 'Spain', "c")
```

Process the request you want to know:

```ruby
station.forecast
station.current_conditions
station.wind_conditions
station.one_word_conditions
```

The response will come in json format. In this gem the response is not treated and it will be served as it comes from Yahoo directly.
This is an example of `forecast` request:
```json
{
 "query": {
  "count": 1,
  "created": "2017-08-06T18:18:31Z",
  "lang": "es-ES",
  "results": {
   "channel": {
    "units": {
     "distance": "km",
     "pressure": "mb",
     "speed": "km/h",
     "temperature": "C"
    },
    "title": "Yahoo! Weather - Tarragona, Catalonia, ES",
    "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-775246/",
    "description": "Yahoo! Weather for Tarragona, Catalonia, ES",
    "language": "en-us",
    "lastBuildDate": "Sun, 06 Aug 2017 08:18 PM CEST",
    "ttl": "60",
    "location": {
     "city": "Tarragona",
     "country": "Spain",
     "region": " Catalonia"
    },
    "wind": {
     "chill": "82",
     "direction": "135",
     "speed": "22.53"
    },
    "atmosphere": {
     "humidity": "71",
     "pressure": "34337.99",
     "rising": "0",
     "visibility": "25.91"
    },
    "astronomy": {
     "sunrise": "6:56 am",
     "sunset": "9:5 pm"
    },
    "image": {
     "title": "Yahoo! Weather",
     "width": "142",
     "height": "18",
     "link": "http://weather.yahoo.com",
     "url": "http://l.yimg.com/a/i/brand/purplelogo//uh/us/news-wea.gif"
    },
    "item": {
     "title": "Conditions for Tarragona, Catalonia, ES at 07:00 PM CEST",
     "lat": "41.114689",
     "long": "1.25026",
     "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-775246/",
     "pubDate": "Sun, 06 Aug 2017 07:00 PM CEST",
     "condition": {
      "code": "30",
      "date": "Sun, 06 Aug 2017 07:00 PM CEST",
      "temp": "27",
      "text": "Partly Cloudy"
     },
     "forecast": [
      {
       "code": "30",
       "date": "06 Aug 2017",
       "day": "Sun",
       "high": "31",
       "low": "23",
       "text": "Partly Cloudy"
      },
      {
       "code": "4",
       "date": "07 Aug 2017",
       "day": "Mon",
       "high": "29",
       "low": "23",
       "text": "Thunderstorms"
      },
      {
       "code": "4",
       "date": "08 Aug 2017",
       "day": "Tue",
       "high": "31",
       "low": "25",
       "text": "Thunderstorms"
      },
      {
       "code": "30",
       "date": "09 Aug 2017",
       "day": "Wed",
       "high": "27",
       "low": "23",
       "text": "Partly Cloudy"
      },
      {
       "code": "34",
       "date": "10 Aug 2017",
       "day": "Thu",
       "high": "30",
       "low": "18",
       "text": "Mostly Sunny"
      },
      {
       "code": "30",
       "date": "11 Aug 2017",
       "day": "Fri",
       "high": "27",
       "low": "18",
       "text": "Partly Cloudy"
      },
      {
       "code": "34",
       "date": "12 Aug 2017",
       "day": "Sat",
       "high": "28",
       "low": "21",
       "text": "Mostly Sunny"
      },
      {
       "code": "32",
       "date": "13 Aug 2017",
       "day": "Sun",
       "high": "28",
       "low": "21",
       "text": "Sunny"
      },
      {
       "code": "34",
       "date": "14 Aug 2017",
       "day": "Mon",
       "high": "28",
       "low": "22",
       "text": "Mostly Sunny"
      },
      {
       "code": "34",
       "date": "15 Aug 2017",
       "day": "Tue",
       "high": "29",
       "low": "23",
       "text": "Mostly Sunny"
      }
     ],
     "description": "<![CDATA[<img src=\"http://l.yimg.com/a/i/us/we/52/30.gif\"/>\n<BR />\n<b>Current Conditions:</b>\n<BR />Partly Cloudy\n<BR />\n<BR />\n<b>Forecast:</b>\n<BR /> Sun - Partly Cloudy. High: 31Low: 23\n<BR /> Mon - Thunderstorms. High: 29Low: 23\n<BR /> Tue - Thunderstorms. High: 31Low: 25\n<BR /> Wed - Partly Cloudy. High: 27Low: 23\n<BR /> Thu - Mostly Sunny. High: 30Low: 18\n<BR />\n<BR />\n<a href=\"http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-775246/\">Full Forecast at Yahoo! Weather</a>\n<BR />\n<BR />\n(provided by <a href=\"http://www.weather.com\" >The Weather Channel</a>)\n<BR />\n]]>",
     "guid": {
      "isPermaLink": "false"
     }
    }
   }
  }
 }
}
```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/dantgn/yahoo_weather.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
