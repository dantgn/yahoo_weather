require 'net/http'
require 'uri'

module YahooWeather
  class WeatherRequest
    # https://developer.yahoo.com/weather/

    BASE_URL = "https://query.yahooapis.com/v1/public/yql?"
    YQL_SELECT_ITEMS = {
        forecast: '*',
        wind_conditions: 'wind',
        current_conditions: 'item.condition',
        one_word_conditions: 'item.condition.text'
    }
    
    def initialize(station, query)
      @station = station
      @query = query
    end

    def call
      yql_query = build_yql_query
      process_request(yql_query)
    end

    private

    def process_request(yql_query)
      uri = URI.parse(BASE_URL)
      request = Net::HTTP::Post.new(uri)
      request.body = URI.encode("q=#{yql_query}&format=json")
      req_options = {
          use_ssl: uri.scheme == "https",
      }

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end

      response.code == "200" ? JSON.parse(response.body) : nil
    end

    def build_yql_query
      query = YQL_SELECT_ITEMS[@query]
      raise "Query { #{@query} } not available!" if query.nil?

      "select #{query} from weather.forecast where u = '#{@station.units}' and woeid in (select woeid" +
          " from geo.places(1) where text=\"#{@station.location}\")"
    end
  end
end