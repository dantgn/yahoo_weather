require 'yahoo_weather/version'
require 'yahoo_weather/services/weather_request'
require 'active_support'
require 'active_support/core_ext'

module YahooWeather
  class Station
    attr_accessor :city, :country, :units

    REQUEST_TYPES = %i[forecast wind_conditions current_conditions one_word_conditions].freeze

    def initialize(city, country = nil, units = 'c')
      @city = city
      @country  = country
      @units = units
    end
    
    REQUEST_TYPES.each do |request_type|
      define_method :"#{request_type}" do
        WeatherRequest.new(self, request_type).call
      end
    end

    def location
      @country.present? ? "#{@city}, #{@country}" : @city
    end
  end
end