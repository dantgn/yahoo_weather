require 'spec_helper'

RSpec.describe YahooWeather do
  it 'has a version number' do
    expect(YahooWeather::VERSION).not_to be nil
  end

  context 'location' do
    it 'sets city and country if both present' do
      station = YahooWeather::Station.new('Tarragona', 'Spain')
      expect(station.location).to eq('Tarragona, Spain')
    end

    it 'sets city if country not present' do
      station = YahooWeather::Station.new('Tarragona')
      expect(station.location).to eq('Tarragona')
    end
  end

  context 'Weather Request call' do
    let(:station) { YahooWeather::Station.new('Tarragona', 'Spain') }
    let(:service) { instance_double("YahooWeather::WeatherRequest")}

    it 'gets called by forecast' do
      expect(YahooWeather::WeatherRequest)
        .to receive(:new).with(station, :forecast).and_return(service)
      expect(service).to receive(:call)

      station.forecast      
    end

    it 'gets called by current_conditions' do
      expect(YahooWeather::WeatherRequest)
        .to receive(:new).with(station, :current_conditions).and_return(service)
      expect(service).to receive(:call)

      station.current_conditions
    end

    it 'gets called by wind_conditions' do
      expect(YahooWeather::WeatherRequest)
        .to receive(:new).with(station, :wind_conditions).and_return(service)
      expect(service).to receive(:call)
      
      station.wind_conditions
    end

    it 'gets called by one_word_conditions' do
      expect(YahooWeather::WeatherRequest)
        .to receive(:new).with(station, :one_word_conditions).and_return(service)
      expect(service).to receive(:call)
      
      station.one_word_conditions
    end
  end
end
