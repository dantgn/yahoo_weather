require 'spec_helper'

RSpec.describe YahooWeather::WeatherRequest do
  let(:station) { YahooWeather::Station.new('Tarragona', 'Spain') }
  let(:service) { described_class.new(station, :forecast) }

  context 'with valid location and query' do
    it 'gets forecast' do
      result = service.call
      expect(result['query']['count']).to eq(1)
    end

    it 'gets current conditions' do
      service = described_class.new(station, :current_conditions)
      result  = service.call
      expect(result['query']['count']).to eq(1)
    end

    it 'gets wind conditions' do
      service = described_class.new(station, :wind_conditions)
      result  = service.call
      expect(result['query']['count']).to eq(1)
    end

    it 'gets wind one word conditions' do
      service = described_class.new(station, :one_word_conditions)
      result  = service.call
      expect(result['query']['count']).to eq(1)
    end
  end

  context 'with invalid location' do
    let(:station) { YahooWeather::Station.new('xxxzzz') }
    let(:service) { described_class.new(station, :forecast) }
    
    it 'finds no results for forecast query' do
      result = service.call
      expect(result['query']['count']).to eq(0)
    end

    it 'finds no results for current conditions query' do
      result = station.current_conditions
      expect(result['query']['count']).to eq(0)
    end

    it 'finds no results for wind conditions query' do
      result = station.wind_conditions
      expect(result['query']['count']).to eq(0)
    end

    it 'finds no results for one word conditions query' do
      result = station.one_word_conditions
      expect(result['query']['count']).to eq(0)
    end
  end

  context 'with invalid query' do
    let(:query) { :wrong_query }
    let(:service) { described_class.new(station, query) }
    it 'raises an error' do
      expect { service.call }.to raise_error("Query { #{query.to_s} } not available!")
    end
  end
end
